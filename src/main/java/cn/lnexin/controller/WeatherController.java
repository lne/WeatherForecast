package cn.lnexin.controller;

import java.sql.SQLException;
import java.util.Arrays;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lnexin.util.DataBaseUtil;
import cn.lnexin.util.WeatherHttpUtil;

@Controller
@EnableAutoConfiguration
@RequestMapping("/")
public class WeatherController {

	@RequestMapping("weather/city")
	@ResponseBody
	public Object getCitysWeather(@RequestParam String city_name) {
		return WeatherHttpUtil.getWeather(city_name);
	}

	@RequestMapping("address/provs")
	@ResponseBody
	public Object getProvList() {
		DataBaseUtil jdbc = new DataBaseUtil();
		jdbc.getConn();
		try {
			return jdbc.selectMore("SELECT DISTINCT(`prov_cn`) FROM `citys`;", null);
		} catch (SQLException e) {
			e.printStackTrace();
			return "获取省份列表失败,请联系管理员!";
		}
	}

	@RequestMapping("address/citys")
	@ResponseBody
	public Object getCitysByProv(@RequestParam String prov_en) {
		DataBaseUtil jdbc = new DataBaseUtil();
		jdbc.getConn();
		String sql = "SELECT * FROM `citys` WHERE `parent_en` LIKE ?;";
		try {
			return jdbc.selectMore(sql, Arrays.asList(prov_en));
		} catch (SQLException e) {
			e.printStackTrace();
			return "获取城市列表失败,请联系管理员!";
		}
	}

	@RequestMapping("address/getcityByen")
	@ResponseBody
	public Object getcityByen(@RequestParam String city_en) {
		DataBaseUtil jdbc = new DataBaseUtil();
		jdbc.getConn();
		String sql = "SELECT * FROM `citys` WHERE `city_en` LIKE ?;";
		try {
			return jdbc.selectMore(sql, Arrays.asList(city_en));
		} catch (SQLException e) {
			e.printStackTrace();
			return "获取城市列表失败,请联系管理员!";
		}
	}

	@RequestMapping("address/getcityBycn")
	@ResponseBody
	public Object getcityBycn(@RequestParam String city_cn) {
		DataBaseUtil jdbc = new DataBaseUtil();
		jdbc.getConn();
		String sql = "SELECT * FROM `citys` WHERE `city_cn` LIKE ?;";
		try {
			return jdbc.selectMore(sql, Arrays.asList(city_cn));
		} catch (SQLException e) {
			e.printStackTrace();
			return "获取城市列表失败,请联系管理员!";
		}
	}
}
