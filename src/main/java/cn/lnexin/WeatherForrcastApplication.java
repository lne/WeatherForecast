package cn.lnexin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import cn.lnexin.controller.WeatherController;

@SpringBootApplication
@ComponentScan
public class WeatherForrcastApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(WeatherController.class);
		
	}
}
