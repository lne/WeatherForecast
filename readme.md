#### 环境要求：
    1.JDK1.8 
    2.基于maven构建的项目
    3.Mysql 数据库,新建库，导入根目录下weather_city.sql文件
    4.启动SpringBoot主程即可;


#### 现在没有前台页面，只有后台数据查询

> 查询省份列表：http://127.0.0.1:8888/address/provs

> 查询省份下属城市：http://127.0.0.1:8888/address/citys?prov_en=beijing

> 根据拼音查询某个城市的信息：http://127.0.0.1:8888/address/getcityByen?city_en=beijing

> 根据中文查询某个城市的信息：http://127.0.0.1:8888/address/getcityByen?city_cn=北京


> 根据中文名称来查询某个城市未来三天的天气信息：http://127.0.0.1:8888/weather/city?city_name=%E5%8D%97%E6%98%8C

> 根据拼音名称来查询某个城市未来三天的天气信息：http://127.0.0.1:8888/weather/city?city_name=shanghai
